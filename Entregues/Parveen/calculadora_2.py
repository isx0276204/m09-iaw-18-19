#!/usr/bin/python
# -*- coding: utf-8 -*


import os
import sys,urllib
import time,cgi
import random
#import cgitb; cgitb.enable()

from genshi.template import TemplateLoader



########################################################################
# Creaci resposta
########################################################################

def pagina(template,environment):
	# template: nom fitxer XML
	# environment: diccionari de valors a usar per la plantilla
	path = ["./","/opt/templates"] # directoris on cercar plantilles
	loader = TemplateLoader(path)
	template = loader.load(template)
	stream = template.generate(**environment) # pasem entorn a plantilla
	#sys.stdout.write("Content-Type: text/html\r\n\r\n")
	sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Calcul i definici variables
########################################################################

plantilla = "calculadora_2.xml"

#sys.stdout.write('Content-Type: text/html\r\n\r\n ok')

########################################################################
# Processament petici
########################################################################

if os.environ['REQUEST_METHOD'] == 'GET':
	# si esperem query string (cal disposar de la funció parse):
	#form = parse(os.environ['QUERY_STRING'])
	# si cal modificar entorn:
	result = False
	m = ''
	n = ''
	operador = ''
	entorn = { 'result' : result,'sm':m,'sn':n,'so':operador }
	# i finalment mostrar el formulari per primera vegada:
	pagina(plantilla,entorn)
else:
	# si esperem dades (cal disposar de la funció parse):
	form = cgi.FieldStorage()
	#form = parse(sys.stdin.read(int(os.environ['CONTENT_LENGTH'])))
	#sys.stdout.write('Content-Type: text/html\r\n\r\n %s' %form['num1'])
	answer = ""
	operador = form.getvalue('oprection')
	m = form.getvalue('num1')
	n = form.getvalue('num2')
	
	if m is None or not(str.isdigit(m)):
		sys.stdout.write('Content-Type: text/html\r\n\r\n %s' % 
		'error1:- numerber 1 is not correct \
		---> help:- choose integer number and select operador again')
		
		result = False
		entorn = { 'result' : result ,'sm':m,'sn':n,'so':operador }
		pagina(plantilla,entorn)
		sys.exit(1)

	if n is None or not(str.isdigit(n)):
		sys.stdout.write('Content-Type: text/html\r\n\r\n %s' % 
		'error2:- numerber 2 is not correct \
		----> help:- choose integer number and select operador again')
		result = False
		entorn = { 'result' : result ,'sm':m,'sn':n,'so':operador }
		pagina(plantilla,entorn)
		sys.exit(1)
	
	if n > m :
		big = n
		n = m
		m = big
	
	if n == '0' and operador == '/' :
		sys.stdout.write('Content-Type: text/html\r\n\r\n %s' % 'error3')
		sys.exit(2)

	#m = int(form.getvalue('num1'))
	#n = int(form.getvalue('num2'))
	
	
		 
	# operamos
	
	try:
		m = float(m)
		n = float(n)

	except ValueError:
		m = 0
		n = 0

	# Comprovem operador
	if operador == '+':
		res = m + n
		operador = '+'

	elif operador == '-':
		res = m - n
		operador = '&minus;'

	elif operador == '*':
		res = m * n
		operador = '&times;'

	elif operador == '/':
		if n != 0:
			res = m / n
			operador = '&divide;'
		else:
			res = -1
			operador = '&divide;'
	else:
		res = m + n
		operador = '+'

	
	res = str(res)
	#sys.stdout.write('Content-Type: text/html\r\n\r\n %s' %str(res))
	#template = "La answer es {answer}"
	#print(template.format(answer=res))
	#sys.exit(0)
	
	entorn = { 'ans' : res,'result': True}
	
	#form = parse(os.environ['QUERY_STRING'])
	#sys.stdout.write(form)
	# validar form:
	# ...
	# si ok acabar (redirigir o respondre...)
	# ...
	# else 
	#   modificar entorn si cal
	#   ...
	# i finalment mostrar el formulari de nou:
	pagina(plantilla,entorn)
sys.exit(0)
