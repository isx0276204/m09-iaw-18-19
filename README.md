<div style="text-align:center;">

![Logo](logo.png)

</div>

# Impantació d'aplicacions web <!-- [MP9](MP9.md)-->
***

-   **UF1**: Llenguatges de guions de servidor<!-- (MP9.md#MP9UF1AC)-->
    1.  Servidors de pàgines dinàmiques <!-- (MP9.md#acMP9UF1A1) -->
        -   [Configurar l’entorn de
            desenvolupament](UF1/A1/T1.md)
        -   [Consultar documentació en línia](UF1/A1/T2.md)
        -   [Inspeccionar scripts amb exemples
            bàsics](UF1/A1/T3.md)
        -   [Programar scripts en l’estil del
            CGI](UF1/A1/T4.md)
        -   [PE1: Conceptes bàsics del protocol
            HTTP](UF1/A1/PE1.md)
        -   [EP1: Programació d’scripts](UF1/A1/EP1.md)

    2.  Sistemes de plantilles Web<!-- (MP9.md#acMP9UF1A2)-->
        -   [Crear plantilles](UF1/A2/T1.md)
        -   [Connectar plantilles amb scripts](UF1/A2/T2.md)
        -   [Crear formularis md](UF1/A2/T3.md)
        -   [PE1: Conceptes bàsics de les plantilles basades en
            XML](UF1/A2/PE1.md)
        -   [EP1: Creació de pàgines dinàmiques](UF1/A2/EP1.md)

    3.  Accés a bases de dades<!-- (MP9.md#acMP9UF1A3)-->
        -   [Gestionar les connexions a la base de
            dades](UF1/A3/T1.md)
        -   [Consultar i actualitzar la base de
            dades](UF1/A3/T2.md)
        -   [Dissenyar la interfície d’usuari](UF1/A3/T3.md)
        -   [PE1: Implementació elemental de bases dades amb
            Python](UF1/A3/PE1.md)
        -   [EP1: Creació de formularis connectats a la base de
            dades](UF1/A3/EP1.md)
-   **UF2**: Implantació de gestors de continguts<!-- (MP9.md#MP9UF2AC)-->
    1.  Tecnologies de suport<!-- (MP9.md#acMP9UF2A1)-->
        -   [Editar continguts amb MarkDown](UF2/A1/T1.md)
        -   [Gestionar repositoris amb Git](UF2/A1/T2.md)
        -   [EP1: Utilització de les tecnologies de
            suport](UF2/A1/EP1.md)

    2.  Preparació dels continguts<!-- (MP9.md#acMP9UF2A2)-->
        -   [Configurar l’entorn de treball](UF2/A2/T1.md)
        -   [Processar continguts](UF2/A2/T2.md)
        -   [EP1: Processament de continguts](UF2/A2/EP1.md)

    3.  Publicació de continguts<!-- (MP9.md#acMP9UF2A3)-->
        -   [Configurar l’entorn de treball](UF2/A3/T1.md)
        -   [Publicar continguts a Internet](UF2/A3/T2.md)
        -   [EP1: Publicació de continguts](UF2/A3/EP1.md)

    4.  Gestors de continguts estàtics<!-- (MP9.md#acMP9UF2A4)-->
        -   [Configurar l’entorn de treball](UF2/A4/T1.md)
        -   [Publicar continguts a Internet](UF2/A4/T2.md)
        -   [EP1: Adaptació del gestor de
            continguts](UF2/A4/EP1.md)
        -   [TR1: Documentació del treballs
            realitzats](UF2/A4/TR1.md)

------------------------------------------------------------------------
