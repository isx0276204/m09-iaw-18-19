Crear formularis HTML
=====================

MP9UF1A2T3

Recollida de dades en pàgines (dinàmiques) HTML

Mòduls `cgi` i `cgitb` de Python
--------------------------------

Per aquestes pràctiques usarem encara la funció `parse` feta a classe, i
no el mòdul `cgi` de Python. De totes formes, el mòdul `cgitb` segueix
ajudant a depurar els scripts.

Per ajudar a la depuració dels scripts usarem aquest codi:

    import cgitb; cgitb.enable()

Una vegada depurats els error es convenient comentar aquesta línia.

Formularis HTML
---------------

En crear formularis amb HTML cal tenir presents els següents aspectes:

-   L’atribut `action` de l’element `form` conté el nom de l’script que
    processarà el formulari.
-   Es recomana fer servir el mètode `post`, en lletres minúscules
    (atribut `method` de l’element `form`).
-   Els boton bàsics per enviar o netejar el formulari es fan amb
    elements `input` de tipus `submit` o `reset`.

Processament de formularis
--------------------------

Treballarem, per processar els formularis, amb la tècnica que anomenarem
**GET/POST**: la *pàgina d’entrada* és el script CGI (que no te per que
tenir extensió); aquest retorna com a resposta el formulari (plantilla)
o el resultat de processar-lo, segons el mètode usat en la petició sigui
`GET` o `POST` o hi hagi errors en les dades.

Observa que el mètode serà `GET` si anem al formulari clicant en un
enllaç normal, i serà `POST` en clicar els botons del formulari (cosa
que forçarem amb l'atribut `method` del formulari). Aquestes són totes
les situacions possibles:

-   El mètode és `GET`: cal mostrar per primera vegada, si cal amb
    valors per defecte pels seus camps (l’usuari ha clicat en una pàgina
    per anar al formulari).
-   El mètode és `POST`: cal validar i processar el formulari (l’usuari
    ha clicat en el botó del formulari)…
    -   … si el formulari no valida s’ha de tornar a presentar aquest a
        l’usuari, amb missatge d’error,
    -   … si el formulari valida correctament s’ha de processar,
        retornant el resultar d’expandir una nova plantilla o,
        alternativament, redirigir a una nova pàgina.

**Important**: per l’usuari *existeix* una sola pàgina!

Aques és l’aspecte general de tots els scripts:

    import os, sys

    from genshi.template import TemplateLoader

    ########################################################################
    # Creació resposta
    ########################################################################

    def pagina(template, environment):
        # template: nom fitxer XML
        # environment: diccionari de valors a usar per la plantilla

        path = ["./", "/opt/templates"] # directoris on cercar plantilles

        loader = TemplateLoader(path)

        template = loader.load(template)

        stream = template.generate(**environment) # pasem entorn a plantilla

        sys.stdout.write("Content-Type: text/html\r\n\r\n")
        sys.stdout.write(stream.render(encoding="UTF-8"))

    ########################################################################
    # Càlcul i definició variables
    ########################################################################

    entorn = {
        'A': ...,
        'B': ...,
        'C': ...,
        ...
    }

    plantila = "nom-plantilla.html"

    ########################################################################
    # Processament petició
    ########################################################################

    if os.environ['REQUEST_METHOD'] == 'GET':
        # si esperem query string (cal disposar de la funció parse):
        #?form = parse(os.environ['QUERY_STRING'])
        # si cal modificar entorn:
        # ...
        # i finalment mostrar el formulari per primera vegada:
        pagina(plantilla, entorn)
    else:
        # si esperem dades (cal disposar de la funció parse):
        form = parse(sys.stdin.read(int(os.environ['CONTENT_LENGTH'])))
        # validar form:
        # ...
        # si ok acabar (redirigir o respondre...)
        # ...
        # else 
        #   modificar entorn si cal
        #   ...
        # i finalment mostrar el formulari de nou:
             pagina(plantilla, entorn)

    sys.exit(0) 

Enllaços recomanats
-------------------

-   En la documentació local de Genshi: [Genshi XML Template
    Language](file:///usr/share/doc/python-genshi/doc/xml-templates.html).

Pràctiques
----------

Sempre utilitzant la tècnica **GET/POST**, implementa aquests formularis
com una pàgina dinàmica, usant en cada cas un script més una plantilla.

1.  Fes un formulari que demani dos nombres. El formulari ha de ser
    processat retornant finalment com a simple text el resultat de sumar
    els dos nombres. Gestiona els casos d’error més evidents (nombres
    absents o malformats), presentant reiteradament el formulari fins
    que l’usuari els superi tots.
2.  Fes un formulari que, en ser processat, si algun dels camps del
    formulari està buit es torni a torni a mostrar però conservant els
    valors dels camps que sí tenien contingut correcte. En rebre tots
    els camps correctament cal retornar una resposta mostrant els valors
    rebuts (en una primera versió retorna una simple cadena de text).
    Estructura general de l’script:
    1.  Si el mètode és `GET`…
    2.  …retornar el resultat de cridar a la plantilla.
    3.  Si el mètode és `POST`…
    4.  …verificar que hem rebut els camps obligatoris
    5.      …i si no els hem rebut tornar a cridar a la plantilla
        conservant els valors del camps que sí hem rebut
    6.      …i si els hem rebut tots retornar resultat (simple text
        o pàgina)


