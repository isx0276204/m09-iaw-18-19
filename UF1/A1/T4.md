Programar scripts en l’estil del CGI
====================================

MP9UF1A1T4

Usos tradicionals del Common Gateway Interface

El Common Gateway Interface
---------------------------

Alguns usos típics del CGI són:

-   Processat de formularis
-   *Banners* i enllaços publicitaris
-   Generació dinàmica de dades, com ara comptadors de visites

*Percent encoding (URL encoding)*
---------------------------------

La cadena de caràcters que en les URLs segueix al caràcter `'?'`,
coneguda com *query string*, està codificada segons les següents
convencions:

-   El seu tipus MIME és `application/x-www-form-urlencoded`
-   Els espais en blanc són substituïts pel caràcter `'+'`
-   Els caràcters amb codi superior a 128, i alguns caràcters ASCII com
    els següents es codifiquen en hexadecimal precedits del caràcter de
    percentatge:
      ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- --------- ------- ------- ------- -------
      `<`     `>`     `~`     `.`     `"`     `{`     `}`     `|`     `\`     `+`     `-`     `` ` ``   `_`     `^`     `%`     espai
      `%3C`   `%3E`   `%7E`   `%2E`   `%22`   `%7B`   `%7D`   `%7C`   `%5C`   `%2B`   `%2D`   `%60`     `%5F`   `%5E`   `%25`   `%20`
      ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- ------- --------- ------- ------- ------- -------

      : Caràcters comuns després de ser codificats

-   La cadena codifica un diccionari organitzant-lo com una llista de
    parelles *clau=valor* separades pel caràcter `'&'`
-   Segons el protocol CGI la cadena queda, sense descodificar, en la
    variable `QUERY_STRING`. De moment, quan sigui necessari, la pots
    convertir a diccionari amb codi similar a aquest:

        import urllib, os

        def parse(s):
            """Parse the url-encoded string s returning a dictionary.
            Caveat: does not support duplicated keys, etc.
            """
            if s == "": return {}
            pairs = s.split("&")
            pairs = [(k, urllib.unquote_plus(v))
                      for (k, v) in [s.split("=", 1) for s in pairs]]
            return dict(pairs)

        form = parse(os.environ["QUERY_STRING"])

Pseudocapçaleres CGI
--------------------

A banda de les capçaleres HTTP legítimes que un script CGI pugui
escriure en la seva sortida estàndard, i que el servidor web reenviarà
al client que ha fet la petició, també disposem de dues falses
capçaleres que en realitat serveixen com a instruccions pel servidor
web:

-   `Status`: definirà el contingut de la primera línia de la resposta
    HTTP (per defecte `200 Ok`).
-   `Location`: farà que el servidor generi una resposta de tipus `300`
    amb la capçalera també copiada en la resposta.

Enllaços recomanats
-------------------

A mes a mes dels presentats en l’activitat dedicada a l’organització de
la documentació:

-   [Common Gateway
    Interface](http://en.wikipedia.org/wiki/Common_Gateway_Interface) a
    la Wikipedia
-   [Percent encoding](http://en.wikipedia.org/wiki/Percent-encoding) a
    la Wikipedia

Pràctiques I (directori *uf1a1t4*)
------------

**Important**: visualitza permanentment els complements de Firefox
All-In-One Sidebar i Live HTTP headers per poder estudiar les capçaleres
HTTP intercanviades entre client i servidor.

-   Fes un script de nom `daus` que en ser visitat retorni el resultat
    de simular una tirada de daus (usa el mòdul de Python `random` per
    obtenir valors a l’atzar).
-   Fes un script de nom `salta` que en ser visitat redirigeixi a una
    URL triada a l’atzar d’una llista (has de redirigir amb la capçalera
    escrita per l’script `Location: URL`). Quin tipus de resposta de
    redirecció ha fet el servidor?
-   Fes un script de nom `calcula` que en ser visitat amb una *query
    string* amb la forma `?N=nombre&M=nombre&OP=operació` retorni el
    resultat d’aplicar l’operació `OP` (suma, resta, multiplicació o
    divisió codificades amb un sol caràcter) als nombres `N` i `M`.
-   En un script de nom `query.py` millora la funció `parse` per que
    suporti noms de camps repetits. No deixis d’afegir comentaris
    justificant les teves decisions i codi de prova, i observa que tan
    sols has de modificar la darrera línia de codi de la funció.
-   Millora la funció `parse` per que, una vegada tallada la cadena pels
    ampersands, verifiqui aquests aspectes: cap bocí és la cadena buida;
    tots els bocins contenen un caràcter `'='`. Si fracasen els tests
    cal acabar amb `raise ValueError('Bad query string')`.
-   Substitueix en la funció `parse` la crida a `urllib.unquote_plus`
    per la funció `decode` desenvolupada a classe.

Pràctiques II
-------------

-   En un script de nom `count.py` implementa un comptador de visites (a
    emmagatzemar en un fitxer a `/var/www/tmp`; dòna permisos
    d’scriptura en el directori al grup `apache`): la resposta ha
    d’informar de les vegades que s’ha visitat la URL.
-   Fa anys van ser populars els
    [Webrings](http://es.wikipedia.org/wiki/Anillo_web). Fes un script
    que en ser visitat amb una *query string* amb la forma
    `?visita=direcció`{style="white-space: nowrap;"} (on *direcció* pot
    ser `esquerra`, `dreta` o `atzar` utilitzi el valor de la capçalera
    HTTP `Referer` (excepte en el cas de la redirecció a l’atzar) per
    decidir a quina pàgina redirigir al visitant de l’anell web. L’anell
    el pots representar en una simple llista que has d’imaginar
    *circular* (posició en la llista anterior a `0`: `len(llista)-1`;
    posició posterior en la llista a `len(llista)-1`: `0`; en altres
    casos simplement incrementar o decrementar la posició en `1`).\
    Pseudocodi del programa:
    1.  Si no es rep la capçalera HTTP `Referer` redirigir a una URL a
        l’atzar de l’anell.
    2.  Si els valors extrets de la *query string* no són correctes
        retornar un missatge d’error.
    3.  En cas contrari i si es demana visitar una pàgina a l’atzar,
        redirigir apropiadament.
    4.  En cas contrari buscar la URL de la capçalera HTTP `Referer` en
        la llista i…:
    5.  … si no es troba retornar un missatge d’error
    6.  … en cas contrari redirigir a la URL anterior o posterior segons
        s’hagi demanat.

    Pensa que necessitaràs dos o més pàgines HTML estàtiques per poder
    provar el sistema d’anell web.\
    Es recomana definir funcions auxiliars per les parts del codi que no
    han de conèixer res sobre el CGI, com ara per buscar dins la llista
    circular, i provar aquestes funcions en un mòdul separat abans de
    fer l’script CGI.
-   Implementa un script per gestionar [Web
    banners](http://es.wikipedia.org/wiki/Banner) (consulta també la
    Wikipedia [anglesa](http://en.wikipedia.org/wiki/Web_banner)).

