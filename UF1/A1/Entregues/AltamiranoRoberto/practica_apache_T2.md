## practica apache T2
### roberto altamirano martinez
### isx47262285
-------------------------------------------------------------

+ Una vegada estudiat mínimament el protocol HTTP, utilitzant el
 programa telnet fes simples peticions al teu servidor HTTP (ordre
 telnet host 80).
 
nos printa por pantalla el contenido de nuestra pantalla!!

 
```
[isx47262285@i15 ~]$ telnet localhost 80
Trying ::1...
Connected to localhost.
Escape character is '^]'.
GET / HTTP/1.0

HTTP/1.1 200 OK
Date: Mon, 11 Mar 2019 10:38:24 GMT
Server: Apache/2.4.34 (Fedora) OpenSSL/1.1.0h-fips
Last-Modified: Tue, 05 Mar 2019 08:32:53 GMT
ETag: "182-58354b8236e82"
Accept-Ranges: bytes
Content-Length: 386
Connection: close
Content-Type: text/html; charset=UTF-8

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca"/>
              <head><title>Salutacions</title></head>
              <body>
                <h1>Salutacions soc roberto </h1>
                <p>Uep!</p>
              </body>
            </html>
Connection closed by foreign host.

```


* utilizando el protocolo 1.1 nos obliga a una nueva entrada:
  > host: robshow.com:80

en este nueva version de protocolo nos admite sedes virtuales para un mismo host!

llegados a este punto hemos de tener registradas estas cedes en '/etc/hosts'
ejemplo:
```
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
127.0.0.1 secure.robshowfm.com
127.0.0.1 robshow.com
~                                                                             
~                              
```

aplicamos los conocimientos:

```

[isx47262285@i15 ~]$ telnet localhost 80
Trying ::1...
Connected to localhost.
Escape character is '^]'.
GET / HTTP/1.1
host: robshow.com:80

HTTP/1.1 200 OK
Date: Mon, 11 Mar 2019 10:49:56 GMT
Server: Apache/2.4.34 (Fedora) OpenSSL/1.1.0h-fips
Last-Modified: Tue, 05 Mar 2019 08:32:53 GMT
ETag: "182-58354b8236e82"
Accept-Ranges: bytes
Content-Length: 386
Content-Type: text/html; charset=UTF-8

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca"/>
              <head><title>Salutacions</title></head>
              <body>
                <h1>Salutacions soc roberto </h1>
                <p>Uep!</p>
              </body>
            </html>
Connection closed by foreign host.
```


+ Verifica que disposes de la instal·lació local de la documentació de Python.

 
[isx47262285@i15 ~]$ rpm -qf $(which python)
python2-2.7.15-3.fc27.x86_64



[isx47262285@i15 ~]$ rpm -ql python2-2.7.15-3.fc27.x86_64
/usr/bin/pydoc
/usr/bin/pydoc2
/usr/bin/pydoc2.7
/usr/bin/python
/usr/bin/python2
/usr/bin/python2.7
/usr/lib/.build-id
/usr/lib/.build-id/b7
/usr/lib/.build-id/b7/0c1261385a87b1affa03664064bdca24baf1f6
/usr/share/doc/python2
/usr/share/doc/python2/README
/usr/share/licenses/python2
/usr/share/licenses/python2/LICENSE
/usr/share/man/man1/python.1.gz
/usr/share/man/man1/python2.1.gz
/usr/share/man/man1/python2.7.1.gz



+ Verifica que pots accedir a tota la resta de documentació local.

podem accedir sense problema a tota la documentacio!


> podem genera un navegador la orden pydoc -p 5000
 y acceder a la url localhost:5000
 
 


