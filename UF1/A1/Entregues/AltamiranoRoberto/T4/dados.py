#!/usr/bin/python
#-*- coding: utf-8-*-
# isx47262285
# ROBERTO ALTAMIRANO MARTINEZ
# descripcion: script que lanza los dados 
########################################################################

import sys
import cgi, random


write = sys.stdout.write
form = cgi.FieldStorage()

# dados random
dado1 = random.randrange(7)
dado2 = random.randrange(7)

resultado = dado1 + dado2
# SORTIDA


PAG = '''<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>daus</title>
	</head>
	<body>
		<p> %s </p>
	</body>
</html>
'''
write("Content-Type: text/html; charset=UTF-8\r\n") 
write("\r\n")

write(PAG % (resultado))

sys.exit(0)
