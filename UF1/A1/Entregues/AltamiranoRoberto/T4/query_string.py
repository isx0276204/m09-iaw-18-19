#!/usr/bin/python
#-*- coding: utf-8-*-
####################################################################

import urllib, os

def parse(s):
    """Parse the url-encoded strings returning a dictionary.
    Caveat: does not support duplicated keys, etc.
    """
    if s == "": return {}
    pairs = s.split("&")
    pairs = [(k, urllib.unquote_plus(v))
              for (k, v) in [s.split("=", 1) for s in pairs]]
    return dict(pairs)

form = parse(os.environ["QUERY_STRING"])
