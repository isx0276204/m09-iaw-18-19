#!/usr/bin/python
#-*- coding: utf-8-*-
# isx47262285
# ROBERTO ALTAMIRANO MARTINEZ	
# descripcion: script que simula una calculadora 
#		
#####################################################

'''Test CGI script.
'''

import sys
import cgi
import cgitb; cgitb.enable()
import query_string, os 

# Debug
#import cgitb; cgitb.enable()
resultado = 0

write = sys.stdout.write

form = cgi.FieldStorage()

# variables

dic_valors = query_string.parse(os.envirom["QUERY_STRING"])

valor1 = dic_valors['N']
valor2 = dic_valors['M']

operator = dic_valors['OP']

# control de errores 

if valor1 is None:
	sys.stderr.write("no data")
	sys.stderr.write("\r\n")
	
	valor1 = 0

if valor2 is None:
	sys.stderr.write("no data")
	sys.stderr.write("\r\n")
	
	valor2 = 0


try:
	valor1 = float(valor1)
	valor2 = float(valor2)
	
except ValueError:
	valor1 = 0
	valor2 = 0
	sys.stderr.write("Entrades no valides per a la calculadora.")
	sys.stderr.write("\r\n")

	
#operacion:
if operator == "resta":
	resultado = valor1 - valor2
elif operator == "suma":
	resultado = valor1 + valor2
elif operator == "multiplicacion":
	resultado = valor1 * valor2
elif operator == "division" and valor2 != 0:
	resultado = valor1 / valor2
else:
	resultado = "error:  no se puede dividir entre 0 "

########## pagina de retorno a la calculadora#########

PAG = '''<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Respuesta calculadora</title>
	</head>
	<body>
		<p> El resultado de %d %s %d es: %d </p>
		</br>
		<a href="/formulario_calculadora.html"> Calculadora </a>
	</body>
</html>
'''


write(PAG %(valor1,operator,valor2,resultado))

sys.exit(0)
