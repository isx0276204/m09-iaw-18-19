#!/usr/bin/python
#-*- coding: utf-8-*-
# isx47262285
# ROBERTO ALTAMIRANO MARTINEZ
# descripcion: script que hace un salto a paginas web random 
#########################################################################
import random
import os, sys

write = sys.stdout.write

#lista de direcciones url al azar
llista_URL = ['www.sport.es','www.fedoraproject.org','www.escoladeltreball.org','www.maxima.fm']

# utilizamos el metodo random para cojer una pag de la lista predefinida
URL = random.sample(llista_URL,1)

# Capçalera location
write('Content-Type: text/plain; charset=UTF-8\r\n')
write('Location: http://%s \r\n' %(URL[0]))
write('\r\n')
