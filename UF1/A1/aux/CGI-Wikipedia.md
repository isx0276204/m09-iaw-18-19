<div id="mw-page-base" class="noprint">

</div>

<div id="mw-head-base" class="noprint">

</div>

<div id="content">

[](){#top}
<div id="mw-js-message" style="display:none;">

</div>

<div id="siteNotice">

</div>

Interfaz de entrada común {#firstHeading .firstHeading}
=========================

<div id="bodyContent">

<div id="siteSub">

De Wikipedia, la enciclopedia libre

</div>

<div id="contentSub">

</div>

<div id="jump-to-nav">

Saltar a [navegación](#mw-head), [búsqueda](#p-search)

</div>

**Interfaz de entrada común** (en
[inglés](/wiki/Idioma_ingl%C3%A9s "Idioma inglés") *Common Gateway
Interface*, abreviado **CGI**) es una importante tecnología de la [World
Wide Web](/wiki/World_Wide_Web) que permite a un cliente ([navegador
web](/wiki/Navegador_web)) solicitar datos de un programa ejecutado en
un [servidor web](/wiki/Servidor_web). CGI especifica un
[estándar](/wiki/Norma_(tecnolog%C3%ADa) "Norma (tecnología)") para
transferir datos entre el cliente y el programa. Es un mecanismo de
comunicación entre el servidor web y una aplicación externa cuyo
resultado final de la ejecución son objetos
[MIME](/wiki/MIME "MIME"){.mw-redirect}. Las aplicaciones que se
ejecutan en el servidor reciben el nombre de CGIs.

Las aplicaciones CGI fueron una de las primeras prácticas de crear
[contenido dinámico](/wiki/Contenido_din%C3%A1mico) para las [páginas
web](/wiki/P%C3%A1gina_web "Página web"). En una aplicación CGI, el
[servidor](/wiki/Servidor) web pasa las solicitudes del
[cliente](/wiki/Cliente_(inform%C3%A1tica) "Cliente (informática)") a un
programa externo. Este programa puede estar escrito en cualquier
lenguaje que soporte el servidor, aunque por razones de portabilidad se
suelen usar [lenguajes de
script](/wiki/Lenguaje_interpretado "Lenguaje interpretado"){.mw-redirect}.
La salida de dicho programa es enviada al cliente en lugar del archivo
estático tradicional.

CGI ha hecho posible la implementación de funciones nuevas y variadas en
las páginas web, de tal manera que esta interfaz rápidamente se volvió
un estándar, siendo implementada en todo tipo de servidores web.

+--------------------------------------------------------------------------+
| <div id="toctitle">                                                      |
|                                                                          |
| Contenido                                                                |
| ---------                                                                |
|                                                                          |
| </div>                                                                   |
|                                                                          |
| -   [<span class="tocnumber">1</span> <span class="toctext">Forma de     |
|     actuación de CGI</span>](#Forma_de_actuaci.C3.B3n_de_CGI)            |
| -   [<span class="tocnumber">2</span> <span class="toctext">Programación |
|     de un CGI</span>](#Programaci.C3.B3n_de_un_CGI)                      |
| -   [<span class="tocnumber">3</span> <span class="toctext">Intercambio  |
|     de información: Variables de                                         |
|     entorno</span>](#Intercambio_de_informaci.C3.B3n:_Variables_de_entor |
| no)                                                                      |
| -   [<span class="tocnumber">4</span> <span class="toctext">Tipos        |
|     habituales de CGIs</span>](#Tipos_habituales_de_CGIs)                |
| -   [<span class="tocnumber">5</span> <span class="toctext">Escenario de |
|     activación de un                                                     |
|     CGI</span>](#Escenario_de_activaci.C3.B3n_de_un_CGI)                 |
| -   [<span class="tocnumber">6</span> <span class="toctext">Véase        |
|     también</span>](#V.C3.A9ase_tambi.C3.A9n)                            |
| -   [<span class="tocnumber">7</span> <span                              |
|     class="toctext">Bibliografía</span>](#Bibliograf.C3.ADa)             |
| -   [<span class="tocnumber">8</span> <span class="toctext">Enlaces      |
|     externos</span>](#Enlaces_externos)                                  |
+--------------------------------------------------------------------------+

<span id="Forma_de_actuaci.C3.B3n_de_CGI" class="mw-headline">Forma de actuación de CGI</span>
----------------------------------------------------------------------------------------------

A continuación se describe la forma de actuación de un CGI de forma
esquemática:

1.  En primera instancia, el servidor recibe una petición (el cliente ha
    activado un [URL](/wiki/URL "URL"){.mw-redirect} que contiene el
    CGI), y comprueba si se trata de una invocación de un CGI.
2.  Posteriormente, el servidor prepara el entorno para ejecutar
    la aplicación. Esta información procede mayoritariamente
    del cliente.
3.  Seguidamente, el servidor ejecuta la aplicación, capturando su
    [salida
    estándar](/w/index.php?title=Salida_est%C3%A1ndar&action=edit&redlink=1 "Salida estándar (aún no redactado)"){.new}.
4.  A continuación, la aplicación realiza su función: como consecuencia
    de su actividad se va generando un objeto
    [MIME](/wiki/MIME "MIME"){.mw-redirect} que la aplicación escribe en
    su salida estándar.
5.  Finalmente, cuando la aplicación finaliza, el servidor envía la
    información producida, junto con información propia, al cliente, que
    se encontraba en estado de espera. Es responsabilidad de la
    aplicación anunciar el tipo de objeto MIME que se genera
    (campo CONTENT\_TYPE).

<span id="Programaci.C3.B3n_de_un_CGI" class="mw-headline">Programación de un CGI</span>
----------------------------------------------------------------------------------------

Un programa CGI puede ser escrito en cualquier lenguaje de programación
que produzca un fichero ejecutable. Entre los lenguajes más habituales
se encuentran:
[C](/wiki/Lenguaje_de_programaci%C3%B3n_C "Lenguaje de programación C"){.mw-redirect},
[C++](/wiki/C%2B%2B), [Perl](/wiki/Perl),
[Java](/wiki/Lenguaje_de_programaci%C3%B3n_Java "Lenguaje de programación Java"){.mw-redirect},
[Visual Basic](/wiki/Visual_Basic)... No obstante, debido a que el CGI
recibe los parámetros en forma de texto será útil un lenguaje que
permita realizar manipulaciones de las cadenas de caracteres de una
forma sencilla, como por ejemplo Perl. Perl es un lenguaje interpretado
que permite manipulaciones sencillas de ficheros y textos, así como la
extracción y manipulación de cadenas de caracteres, unidas a unas
búsquedas rápidas y fáciles.

<span id="Intercambio_de_informaci.C3.B3n:_Variables_de_entorno" class="mw-headline">Intercambio de información: Variables de entorno</span>
--------------------------------------------------------------------------------------------------------------------------------------------

Variables de entorno que se intercambian de cliente a CGI:

1.  **QUERY\_STRING**: Es la cadena de entrada del CGI cuando se utiliza
    el método GET sustituyendo algunos símbolos especiales por otros.
    Cada elemento se envía como una pareja Variable=Valor. Si se utiliza
    el método POST esta variable de entorno está vacía.
2.  **CONTENT\_TYPE**: Tipo MIME de los datos enviados al CGI
    mediante POST. Con GET está vacía. Un valor típico para esta
    variable es: *Application/X-www-form-urlencoded*.
3.  **CONTENT\_LENGTH**: Longitud en bytes de los datos enviados al CGI
    utilizando el método POST. Con GET está vacía.
4.  **PATH\_INFO**: Información adicional de la ruta (el "path") tal y
    como llega al servidor en el URL.
5.  **REQUEST\_METHOD**: Nombre del método (GET o POST) utilizado para
    invocar al CGI.
6.  **SCRIPT\_NAME**: Nombre del CGI invocado.
7.  **SERVER\_PORT**: Puerto por el que el servidor recibe la conexión.
8.  **SERVER\_PROTOCOL**: Nombre y versión del protocolo en uso. (Ej.:
    *HTTP/1.0 o 1.1*)

Variables de entorno que se intercambian de servidor a CGI:

1.  **SERVER\_SOFTWARE**: Nombre y versión del software servidor de www.
2.  **SERVER\_NAME**: Nombre del servidor.
3.  **GATEWAY\_INTERFACE**: Nombre y versión de la interfaz de
    comunicación entre servidor y aplicaciones CGI/1.12

<span id="Tipos_habituales_de_CGIs" class="mw-headline">Tipos habituales de CGIs</span>
---------------------------------------------------------------------------------------

1.  **Contador de accesos**: Cuenta el número de veces que se ha
    solicitado una página determinada. Se guarda el valor en un fichero.
    Cada vez que se invoca se incrementa, para su
    posterior visualización.
2.  **Buscador**: Localiza páginas que contengan los
    términos especificados. Utiliza una tabla que enumera las palabras y
    para cada una especifica las páginas dónde se encuentra.
3.  **Correo**: Obtiene información estructurada del usuario.
4.  **Contribuciones**: Permite añadir enlaces o anotaciones a una
    página, indicando la procedencia de la adición.
5.  **Estadísticas de uso**: Presenta información sobre los
    acontecimientos producidos en el servidor de WWW. El servidor
    mantiene un registro (log) de los acontecimientos que se
    han producido.
6.  **Administración remota del servidor**: Permite interactuar con el
    servidor desde WWW. Invoca los programas que controlan o modifican
    el comportamiento del servidor.

<span id="Escenario_de_activaci.C3.B3n_de_un_CGI" class="mw-headline">Escenario de activación de un CGI</span>
--------------------------------------------------------------------------------------------------------------

1.  **Situación inicial:** El cliente solicita la invocación de un CGI,
    bien de manera involuntaria (se envía únicamente información
    de cabecera) o bien de forma explícita (formulario). En el
    formulario hay parejas del tipo variable=valor. El método de http
    especificado en el formulario puede ser GET o POST.\
    En el servidor en cambio, el fichero de configuración especifica un
    directorio cgi-bin con capacidad para ejecutar programas. Puede
    haber otros ficheros y otros programas a los que puede acceder tanto
    el servidor como sus CGIs.
2.  **El cliente pulsa el botón de tipo SUBMIT en el formulario:**
    Dependiendo del método se construye un mensaje que contiene la
    información del formulario en la cabecera (para GET) o en el cuerpo
    del mensaje (para POST). El mensaje se envía al servidor, añadiendo
    información propia del cliente que el propio navegador conoce. El
    cliente queda a la espera de recibir un objeto MIME como respuesta
    del servidor.
3.  **El servidor recibe el mensaje de petición o pone en marcha el
    programa CGI:** El servidor compara la información del mensaje con
    la que conoce de su fichero de configuración, determinando así la
    validez de la petición. En realidad el servidor se pregunta: ¿Existe
    esta URL? ¿Se tienen todos los permisos?.\
    Prepara el entorno añadiendo información propia a la comunicada por
    el navegador del cliente. Si es GET, la información procedente del
    formulario (parejas variable=valor) se definen en QUERY\_STRING. El
    servidor posteriormente pone en funcionamiento el CGI. Si se trata
    de POST, la información se coloca en la entrada estándar del CGI.
    Finalmente se inicia la ejecución del CGI y el servidor espera a que
    ésta acabe.
4.  **Ejecución del CGI:** El CGI accede a las variables de entorno.
    Comprueba o adapta el funcionamiento según el método GET o POST
    establecido en REQUEST\_METHOD: si se tratara de GET, la información
    estará en QUERY\_STRING, mientras que si se trata de POST, se tomará
    la entrada estándar.

Se construye un objeto MIME que se enviará al cliente. La primera
escritura deberá anunciar el tipo de objeto: CONTENT\_TYPE:
tipo/subtipo.

1.  **El servidor vuelve al trabajo:** El servidor añade a su respuesta
    del CGI una cabecera indicando su tamaño (CONTENT\_LENGTH).
2.  **El cliente recibe la respuesta:** Interpretación de la respuesta.
    Visualización con el navegador.

</div>

</div>
