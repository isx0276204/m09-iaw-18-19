Editar continguts amb MarkDown
==============================

MP9UF2A1T1

Edició de continguts amb MarkDown

Us de simple text amb convencions estructurals
----------------------------------------------

Una alternativa pels autors que han de crear documents XHTML o DocBook i
volen concentrar-se en els continguts i no haver de pensar en el marcat
són els sistemes fonamentats en crear simple text però amb convencions
estructurals. Alguns dels més coneguts són aquests:

-   [Markdown](http://daringfireball.net/projects/markdown/syntax)
-   [AsciiDoc](http://www.methods.co.nz/asciidoc/)
-   [reStructuredText](http://docutils.sourceforge.net/rst.html)
-   [Pandoc](http://johnmacfarlane.net/pandoc/README.html)

El més senzill i popular d’aquests sistemes és Markdown, que es pot
resumir en una sola pàgina com ara aquestes:

-   [packetlife.net](http://packetlife.net/media/library/16/Markdown.pdf)
    ([local](aux/Markdown-1.pdf))
-   [Mark
    Boszko](http://stationinthemetro.com/wp-content/uploads/2013/04/Markdown_Cheat_Sheet_v1-1.pdf)
    ([local](aux/Markdown-2.pdf))

Atenció: existeixen moltes variants de Markdown, que afegeixen diferents
tipus d’ampliacions (com ara la utilitzada a
[GitHub](https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet));
nosaltres treballarem sempre que poguem amb la versió original, la més
simple de totes. Una important influència en el disseny de Markdown la
va proporcionar [Aaron Swartz](http://www.aaronsw.com/weblog/001189),
famós hacker i [activista](http://www.aaronsw.com/) per les llibertats a
Internet.

Per convertir fitxers Markdown a XHTML existeix moltes eines. La més
completa és [Pandoc](http://johnmacfarlane.net/pandoc/README.html), i és
la que farem servir.

Pràctiques
----------

1.  Instal·la el paquet `pandoc`:

         MANAGER=yum
            which dnf >/dev/null 2>&1 && MANAGER=dnf
            sudo $MANAGER -y install pandoc texlive

2.  Utilitzant `pandoc` converteix qualsevol exemple de MarkDown a HTML,
    PDF, ODT, etc.
3.  Instal·la `git`:

         MANAGER=yum
            which dnf >/dev/null 2>&1 && MANAGER=dnf
            sudo $MANAGER -y install git gitg meld

4.  Explora el repositori de GitHub
    [fadado/gitet](https://github.com/fadado/gitet). En particular, fes
    una còpia local tal com s’indica més avall i visita la presentació
    sobre MarkDown del directori `slides`.
5.  Explora el repositori de GitHub
    [fadado/literate](https://github.com/fadado/literate).

Per tenir còpies locals dels repositoris de Git mencionats has de
instal·lar `git` i clonar els repositoris:

     mkdir ~/GitHub  # per exemple
        cd ~/GitHub
        git clone https://github.com/fadado/gitet.git
        git clone https://github.com/fadado/literate.git

Explorar els repositoris ho pots fer a GitHub, i en les còpies locals
amb `gitg`, l’explorador de fitxers del sistema, etc.
